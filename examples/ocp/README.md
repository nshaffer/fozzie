One-Component Plasma Example
============================
This example infile configures the solution of the OZ equations for a one-component plasma (OCP) with coupling strength `Γ=10`. Assuming the `foz` executable is in its original directory (two levels up from here), this example can be executed by running

    ../../foz -C ex_ocp.in
    
After the program completes, you can inspect the data files ending in `.dat`. Each will have two columns. The first is the distance (or wavenumber for k-space quantities) in units of the total interparticle spacing `a=∛(3/4πn)`. The other is the pairwise function named in the file (e.g., `g(r)`). For example, the columns of `gr.dat` are `r/a g`. Other data files follow the same format.

Plot the radial distribution function with your favorite plotting software. Does it reach its `r→∞` limit (`g→1`) on the grid of `r` values used? Is there a well-resolved "Coulomb hole," where `g(r)≈0`? Open the input file in a text editor and locate the section headed by "--PLAN". Are the smallest and largest plotted `r` values on the plot consistent with the grid described in the input file?

Plot the partial static structure factor (`Sk.dat`) using a logarithmic scale for the wavenumber. Are there clear `k→0` and `k→∞` limits? What are the smallest and largest `k` values? Are these consistent with the `r` values? The two meshes are related by the constraint that

    k_min r_max = k_max r_min = π
    
as dictated by the Nyquist sampling theorem.

Make a copy of the input file for experimentation and give it a different name. Open it in a text editor and locate the section labeled "--POTENTIALS". This is where the interaction potential parameters are set (most importantly, the coupling strength). Change the value of the coupling strength to `Γ=100` and perform a new run with your edited input file. Compare `g(r)` and `S(k)` to the previous results. Are the asymptotic limits of each still well-resolved and correct? If not, adjust either the mesh spacing or number of points in the "--PLAN" section and rerun. Repeat with `Γ=0.001`. Which parts of `r`- and `k`-space are difficult to resolve at weak coupling versus strong coupling?
