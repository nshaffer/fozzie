Binary Ionic Mixture Example
============================
This example infile configures the solution of the OZ equations for a binary ionic mixture (BIM) composed of 70% hydrogen ions (`Z=1`) and 30% helium ions (`Z=2`). The concentrations refer to mole fraction, not mass fraction. All interactions between particles are Coulombic and are parameterized such that the BIM coupling parameter
           
           1/3   5/3     e²
    Γ = <Z>    <Z   > ------- = 10
                      4πε₀akT
  
where angle brackets denote a concentration-weighted sum over species. Verify that the potential coefficients in `ex_bim.in` are consistent with this. (Hint: the charge-dependent prefactor should work out to be about 1.8) Make sure you understand why these coefficients are different for each species pair, and why they are not just 10.

Assuming the `foz` executable is in its original directory (two levels up from here), this example can be executed by running

    ../../foz -C ex_bim.in
	
After the program completes, you can inspect the data files ending in `.dat`. Each will have five columns. The first is the distance (or wavenumber for k-space quantities) in units of the total interparticle spacing `a=∛(3/4πn)`. The remaining four columns are the pairwise functions. For example, the columns of `gr.dat` are `r/a g_11 g_12 g_21 g_22`. Other data files follow the same format.

Plot the radial distribution functions with your favorite plotting software. Which one has the highest peak? Which one has the widest "Coulomb hole" (region where `g(r)≈0`)? Do all three reach their `r→∞` limit (`g→1`) on the grid of `r` values used?

Plot the partial static structure factors (`Sk.dat`) using a logarithmic scale for the wavenumber. Are there clear `k→0` and `k→∞` limits for each?
