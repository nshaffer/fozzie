Installation
============

Prerequisites
-------------

Fozzie is a Fortran program and thus it needs a Fortran compiler to build. So far, it has only been built with GNU Fortran. Any recent version of gfortran (>4.8) should suffice. Any other recent Fortran compiler (e.g., ifort, pgfortran) should also work, but have not yet been tested.

Fozzie depends on an external library called DFFTPACK. Instructions for obtaining and installing DFFTPACK are provided in this README.

The (optional) helper tools packaged with Fozzie require a Python 2 interpreter. You can check your version of Python by running `python --version` at the command line.

The instructions here assume a Mac OS or Linux environment. If you successfully build Fozzie on Windows, please update this README with instructions on how to do so.

Obtaining the Source Code
-------------------------
The source code for Fozzie is hosted at [Bitbucket](https:/bitbucket.org/nshaffer/fozzie). It can be downloaded either as a zip archive or as a full Git repository. Both methods let you build and run Fozzie. Both methods also let you modify the source code or write new tools. However, if you want your modifications to be part of Fozzie for future users to download, you need to clone the repository.

To get just the source, click the "Downloads" link on the web page's sidebar and then click "Download repository". Extract the contents of the zip archive to the directory of your choice.

To clone the repository, open a terminal on your machine (Mac OS or Linux) and navigate to the directory where you want to download. Then run the command `git clone https://bitbucket.org/nshaffer/fozzie`.

Obtaining and Installing DFTTPACK
---------------------------------
DFFTPACK is a double-precision library for fast Fourier transforms. It can be downloaded from the [Netlib software repository](http://www.netlib.org/fftpack/dp.tgz). If you are using a command-line envirnment, you can download and extract the DFFTPACK source by running

    wget http://www.netlib.org/fftpack/dp.tgz
    tar -xzvf dp.tgz

after which, you should see a directory `dfftpack` containing several ".f" files as well as a "Makefile".  Open the Makefile in a text editor. Make sure the variables `FC` ("Fortran compiler") and `FFLAGS` ("Fortran compilation flags") are appropriate for your Fortran compiler. For instance:

    FC=gfortran
    FFLAGS=-O2 -Wall -Wextra -fPIC
    
is appropriate for compiling with gfortran. After making any necessary changes, you can compile DFFTPACK by running `make` at the command line. There should now be several ".o" files in the directory as well as one file named `libdfftpack.a`, which is the compiled library. You may move it to any directory you please, just take note of where it is.

Compiling Fozzie
----------------
With DFFTPACK installed, return to the Fozzie directory and open the Makefile in a text editor. If necessary, edit the variable `FC` to match the compiler used to build DFFTPACK. Next, edit the variable `LDLIBS` to reflect the directory where you installed DFFPACK. For example, if you placed `libdfftpack.a` in `/home/johndoe/mylibs/`, your `LDLIBS` line should read

    LDLIBS=-L/home/johndoe/mylibs -ldfftpack
    
No other parts of the Makefile need be edited unless you know what you are doing. At this stage, the Fozzie directory should have at a minimum:

- a directory named `src` containing several ".f90" files.
- an empty directory named `mod`
- the edited Makefile

To build Fozzie, run `make`. If the build succeeds, there should be a new executable file named `foz`. Mission accomplished.

Getting Started
===============
In order to set up and run a solution, Fozzie needs to be passed a configuration file. Example input files for various physical systems can be found in the `examples` directory. Each contains a premade configuration file as well as a README that describes the calculation and walks the user through some exercises.

There is an incomplete user guide that can be found in the `guide` directory. It is terribly underdeveloped, but it dissects the format of configuration files in more detail than is presented in the examples.

Runs are mainly configured through the input text file, but Fozzie also has a few command-line options that can be passed. In the directory where the `foz` executable is located, run

    ./foz --help
    
for a synopsis of the command-line options. 

License
=======

Fozzie is distributed under the MIT license, see the contents of `LICENSE.txt`.
