module radialft
!!!
!!!            4pi  /oo
!!!    F(k) =  ---  | dr r sin(kr) f(r)  
!!!             k   /0
!!!
!!!              1    /oo
!!!    f(r) =  -----  | dk k sin(kr) F(k)  
!!!            2pi^2  /0
!!!  
  use kinds, only: dp
  use constants, only: pi
  implicit none
  
  interface
     subroutine DSINTI(n, wsave)
       import dp
       integer :: n
       real(dp) :: wsave(5*n/2+15)
     end subroutine DSINTI

     subroutine DSINT(n, f, wsave)
       import dp
       integer, intent(in) :: n
       real(dp), intent(inout) :: f(n), wsave(5*n/2+15)
     end subroutine DSINT
  end interface

  type rft_t
     integer :: n
     real(dp) :: dr
     real(dp) :: dk
     real(dp), dimension(:), allocatable :: wsave
  end type rft_t
  
contains
  
  subroutine rft_init(plan, n, dr, dk)
    !!
    type(rft_t), intent(inout) :: plan
    integer, intent(in) :: n
    real(dp), intent(in) :: dr, dk

    plan%n = n
    plan%dr = dr
    plan%dk = dk
    
    if (allocated(plan%wsave)) deallocate(plan%wsave)
    allocate(plan%wsave(5*n/2+15))
    call DSINTI(n, plan%wsave)
  end subroutine rft_init

  subroutine rft_final(plan)
    !!
    type(rft_t), intent(inout) :: plan
    if (allocated(plan%wsave)) deallocate(plan%wsave)
  end subroutine rft_final

  subroutine rft(f, plan)
    !!
    type(rft_t), intent(inout) :: plan
    real(dp), dimension(plan%n), intent(inout) :: f
    integer :: i
    
    do i = 1, plan%n
       f(i) = (i*plan%dr)*f(i)
    end do

    call DSINT(plan%n, f, plan%wsave)
    f = (4.0_dp*pi)*(0.5_dp*plan%dr)*f

    do i = 1, plan%n
       f(i) = f(i)/(i*plan%dk)
    end do

  end subroutine rft

  subroutine irft(f, plan)
    !!
    type(rft_t), intent(inout) :: plan
    real(dp), dimension(plan%n), intent(inout) :: f
    integer :: i
    
    do i = 1, plan%n
       f(i) = (i*plan%dk)*f(i)
    end do

    call DSINT(plan%n, f, plan%wsave)
    f = (0.5_dp*plan%dk)*f / (2.0_dp*pi*pi)

    do i = 1, plan%n
       f(i) = f(i)/(i*plan%dr)
    end do

  end subroutine irft

end module radialft
