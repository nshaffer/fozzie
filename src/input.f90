module input
  use kinds, only: dp
  use constants, only: pi
  use radialft
  use potentials
  use closure
  use species
  use iteration_config
  implicit none
  private

  public :: read_config
  public :: OZ_NAME_LEN, CLOSURE_NAME_LEN !FIXME -- find a way not to expose these

  integer, parameter :: CLOSURE_NAME_LEN = 8
  integer, parameter :: OZ_NAME_LEN = 8

  integer, parameter :: INPUT_BAD_FNAME = 1
  integer, parameter :: INPUT_BAD_CONC = 2
  integer, parameter :: INPUT_BAD_ALLOC = 3

contains

  subroutine read_config(fname, iter_config, plan, species, pots, closures, stat)
    character(len=*), intent(in) :: fname
    type(iteration_config_t), intent(out) :: iter_config
    type(rft_t), intent(out) :: plan
    type(species_t), dimension(:), allocatable, intent(out) :: species
    type(potential_t), dimension(:,:), allocatable, intent(out) :: pots
    character(len=CLOSURE_NAME_LEN), dimension(:,:), allocatable, intent(out) :: closures
    integer, intent(out) :: stat

    integer :: unit
    logical :: exists

    stat = 0

    inquire(file=fname, exist=exists)
    if (.not.exists) then
       write(*,*) 'Config file "'//trim(adjustl(fname))//'" does not exist.'
       stat = INPUT_BAD_FNAME
       goto 900
    end if

    open(newunit=unit, file=fname)

    call read_iteration(unit, iter_config, stat)
    if (stat /= 0) then
       write(*,*) 'Error while reading ITERATION.'
       goto 900
    end if
    read(unit,*) ! blank
    call read_plan(unit, plan, stat)
    if (stat /= 0) then
       write(*,*) 'Error while reading PLAN.'
       goto 900
    end if
    read(unit,*) ! blank
    call read_species(unit, species, stat)
    if (stat /= 0) then
       write(*,*) 'Error while reading SPECIES.'
       goto 900
    end if
    read(unit,*) ! blank
    call read_potentials(unit, species, pots, stat)
    if (stat /= 0) then
       write(*,*) 'Error while reading POTENTIALS.'
       goto 900
    end if
    read(unit,*) ! blank
    call read_closures(unit, species, closures, stat)
    if (stat /= 0) then
       write(*,*) 'Error while reading CLOSURES.'
       goto 900
    end if

!!! Cleanup
900 continue
    close(unit)

  end subroutine read_config


  subroutine read_iteration(unit, iter_config, stat)
    integer, intent(in) :: unit
    type(iteration_config_t), intent(out) :: iter_config
    integer, intent(out) :: stat

    character(len=SOLVER_NAME_LEN) :: solver_name
    integer :: max_iterations
    real(dp) :: tolerance, mix

    stat = 0

    read(unit,*,iostat=stat,err=900) ! ITERATION
    read(unit,*,iostat=stat,err=900) solver_name
    read(unit,*,iostat=stat,err=900) max_iterations
    read(unit,*,iostat=stat,err=900) tolerance
    read(unit,*,iostat=stat,err=900) mix
    call init_iteration_config( iter_config, solver_name, max_iterations, tolerance, mix, stat )
    if (stat /= 0) then
       write(*,*) 'Error while initializing iteration configuration.'
       goto 900
    end if

!!! Cleanup
900 continue

    if (stat /= 0) then
       continue
    end if

  end subroutine read_iteration


  subroutine read_plan(unit, plan, stat)
    integer, intent(in) :: unit
    type(rft_t), intent(out) :: plan
    integer, intent(out) :: stat
    integer :: n_pts
    real(dp) :: dr, dk

    stat = 0

    read(unit,*,iostat=stat,err=900) ! PLAN
    read(unit,*,iostat=stat,err=900) n_pts
    read(unit,*,iostat=stat,err=900) dr
    dk = pi/(dr*n_pts)
    call rft_init(plan, n_pts, dr, dk)

!!! Cleanup
900 continue

    if (stat /= 0) then
       call rft_final(plan)
    end if

  end subroutine read_plan


  subroutine read_species(unit, species, stat)
    integer, intent(in) :: unit
    type(species_t), dimension(:), allocatable, intent(out) :: species
    integer, intent(out) :: stat
    integer :: n_sp
    integer :: i, is
    real(dp) :: charge, mass, conc, temperature
    real(dp) :: density, total_conc

    stat = 0

    read(unit,*,iostat=stat,err=900) ! SPECIES
    read(unit,*,iostat=stat,err=900) n_sp

    allocate( species(n_sp) )

    read(unit,*,iostat=stat,err=900) ! i charge mass conc temperature

    total_conc = 0.0_dp
    do i = 1, n_sp
       read(unit,*,iostat=stat,err=900) is, charge, mass, conc, temperature
       total_conc = total_conc + conc
       density = ( 3.0_dp/(4.0_dp*pi) ) * conc
       call init_species( species(is), charge, mass, density, temperature, stat )
       if ( stat /= 0 ) then
          write(*,*) 'Error while initializing species #', is
          goto 900
       end if
    end do
    if ( abs(total_conc - 1.0_dp) > epsilon(1.0_dp) ) then
       stat = INPUT_BAD_CONC
       write(*,*) 'Total concentration must add to 1, is: ', total_conc
       goto 900
    end if

!!! Cleanup
900 continue

    if (stat /= 0) then
       if (allocated(species)) deallocate(species)
    end if

  end subroutine read_species


  subroutine read_potentials(unit, species, pots, stat)
    integer, intent(in) :: unit
    type(species_t), dimension(:), intent(in) :: species
    type(potential_t), dimension(:,:), allocatable, intent(out) :: pots
    integer, intent(out) :: stat

    character(len=POTENTIAL_NAME_LEN) :: potential_name
    integer :: argc
    real(dp), dimension(10) :: argv
    integer :: i, j, is, js

    stat = 0

    allocate(pots(size(species), size(species)), stat=stat)
    if (stat /= 0) then
       stat = INPUT_BAD_ALLOC
       write(*,*) 'Error while allocating potential models.'
       goto 900
    end if

    read(unit,*,iostat=stat,err=900) ! POTENIALS
    read(unit,*,iostat=stat,err=900) ! i j potential argc argv
    do i = 1, size(species)
       do j = 1, size(species)
          argc = 0
          argv(:) = -1.23e300_dp
          read(unit,*,iostat=stat,err=900) is, js, potential_name, argc, argv(1:argc)
          call init_potential( pots(is,js), potential_name, argv )
          if (stat /= 0) then
             write(*,*) 'Error while initializing potential #', is, ',', js
             goto 900
          end if
       end do
    end do

!!! Cleanup
900 continue

    if (stat /= 0) then
       do is = 1, size(species)
          do js = 1, size(species)
             call final_potential(pots(is,js))
          end do
       end do
       if (allocated(pots)) deallocate(pots)
    end if

  end subroutine read_potentials


  subroutine read_closures(unit, species, closures, stat)
    integer, intent(in) :: unit
    type(species_t), dimension(:), intent(in) :: species
    character(len=CLOSURE_NAME_LEN), dimension(:,:), allocatable, intent(out) :: closures
    integer, intent(out) :: stat
    character(len=CLOSURE_NAME_LEN) :: closure_name
    integer :: i, j, is, js

    stat = 0

    allocate( closures(size(species),size(species)), stat=stat )
    if (stat /= 0) then
       write(*,*) 'Error while allocating closure table.'
       goto 900
    end if

    read(unit,*) ! CLOSURES
    read(unit,*) ! i j closure
    do i = 1, size(species)
       do j = 1, size(species)
          read(unit,*) is, js, closure_name
          closures(is,js) = trim(adjustl(closure_name))
       end do
    end do

!!! Cleanup
900 continue

    if (stat /= 0) then
       if (allocated(closures)) deallocate(closures)
    end if

  end subroutine read_closures


end module input
