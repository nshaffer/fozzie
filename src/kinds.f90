module kinds
  implicit none
  private
  public sp, dp
  integer, parameter :: sp = kind(1.e0)
  integer, parameter :: dp = kind(1.d0)
end module kinds
