module potentials
  use kinds, only: dp
  use constants, only: four_pi
  use radialft
  private
  public :: potential_t, init_potential, final_potential, allocate_potential_arrays, tabulate_potentials, POTENTIAL_NAME_LEN

  integer, parameter :: POTENTIAL_NAME_LEN = 12

  integer, parameter :: coulomb_argc = 2
  integer, parameter :: yukawa_argc = 3
  integer, parameter :: lennard_jones_argc = 2
  integer, parameter :: induced_dipole_argc = 4
  integer, parameter :: deutsch_argc = 3
  !integer, parameter :: hard_sphere_argc = 1
  integer, parameter :: coulomb_core_argc = 3

  integer, parameter :: POT_BAD_NAME  = 1
  integer, parameter :: POT_BAD_ALLOC = 2

  type :: potential_t
     character(len=POTENTIAL_NAME_LEN) :: name
     real(dp), dimension(:), allocatable :: argv
  end type potential_t

contains


  subroutine init_potential(pot, name, argv)
    type(potential_t), intent(out) :: pot
    character(len=POTENTIAL_NAME_LEN) :: name
    real(dp), dimension(:), intent(in) :: argv

    call final_potential(pot)

    pot%name = trim(adjustl(name))

    allocate( pot%argv(size(argv)) )
    pot%argv = argv

  end subroutine init_potential


  subroutine final_potential(pot)
    type(potential_t), intent(inout) :: pot

    if (allocated(pot%argv)) deallocate(pot%argv)
  end subroutine final_potential


  subroutine allocate_potential_arrays(num_species, num_gridpoints, ur, uk ,usr, ulk, stat)
    integer, intent(in) :: num_species, num_gridpoints
    real(dp), dimension(:,:,:), allocatable, intent(out) ::ur, uk, usr, ulk
    integer, intent(out) :: stat

    allocate( ur(num_species,num_species,num_gridpoints), stat=stat )
    if (stat /= 0) then
       stat = POT_BAD_ALLOC
       write(*,*) 'Error while allocating ur.'
       goto 900
    else
       ur = -1.23e300_dp
    end if

    allocate( uk(num_species,num_species,num_gridpoints), stat=stat )
    if (stat /= 0) then
       stat = POT_BAD_ALLOC
       write(*,*) 'Error while allocating uk.'
       goto 900
    else
       ur = -1.23e300_dp
    end if

    allocate( usr(num_species,num_species,num_gridpoints), stat=stat )
    if (stat /= 0) then
       stat = POT_BAD_ALLOC
       write(*,*) 'Error while allocating usr.'
       goto 900
    else
       usr = -1.23e300_dp
    end if

    allocate( ulk(num_species,num_species,num_gridpoints), stat=stat )
    if (stat /= 0) then
       stat = POT_BAD_ALLOC
       write(*,*) 'Error while allocating ulk.'
       goto 900
    else
       ulk = -1.23e300_dp
    end if

!!! Cleanup
900 continue

    if (stat /= 0) then
       if (allocated(ur)) deallocate(ur)
       if (allocated(uk)) deallocate(uk)
       if (allocated(usr)) deallocate(usr)
       if (allocated(ulk)) deallocate(ulk)
    end if

  end subroutine allocate_potential_arrays


  subroutine tabulate_potentials(pot, plan, ur, uk, usr, ulk, stat)
    !!
    !! Initialize the short-range r-space potential and long-range
    !! k-space potential for a single pairwise interaction.
    !!
    !! In:
    !!   pot - potential model
    !!
    !! In/Out:
    !!   plan - Radial fourier transform plan
    !!
    !! Out:
    !!   usr - tabulated short-range r-space potential
    !!   ulk - tabulated long-range k-space potential
    !!   stat - 0 on success and an error code otherwise
    !!
    type(potential_t), intent(in) :: pot
    type(rft_t), intent(inout) :: plan
    real(dp), dimension(plan%n), intent(out) :: ur, uk, usr, ulk
    integer, intent(out) :: stat

    integer :: i
    real(dp) :: r, k

    stat = 0

    select case(trim(adjustl(pot%name)))

    case('COULOMB')
       do i = 1, plan%n
          r = i*plan%dr
          k = i*plan%dk
          ur(i) = coulomb_r(r, pot%argv)
          uk(i) = coulomb_k(k, pot%argv)
          usr(i) = coulomb_short_r(r, pot%argv)
          ulk(i) = coulomb_long_k(k, pot%argv)
       end do

    case('YUKAWA')
       do i = 1, plan%n
          r = i*plan%dr
          k = i*plan%dk
          ur(i) = yukawa_r(r, pot%argv)
          uk(i) = yukawa_k(k, pot%argv)
          usr(i) = yukawa_short_r(r, pot%argv)
          ulk(i) = yukawa_long_k(k, pot%argv)
       end do

    case('LJ6-12')
       do i = 1, plan%n
          r = i*plan%dr
          k = i*plan%dk
          ur(i) = lennard_jones_r(r, pot%argv)
          uk(i) = 0.0_dp
          usr(i) = ur(i)
          ulk(i) = 0.0_dp
        end do

        
    case('LJ4-12')
       do i = 1, plan%n
          r = i*plan%dr
          k = i*plan%dk
          ur(i) = induced_dipole_r(r, pot%argv)
          uk(i) = 0.0_dp
          usr(i) = ur(i)
          ulk(i) = 0.0_dp
        end do
        

    case('DEUTSCH')
       do i = 1, plan%n
          r = i*plan%dr
          k = i*plan%dk
          ur(i) = deutsch_r(r, pot%argv)
          uk(i) = deutsch_k(k, pot%argv)
          usr(i) = deutsch_short_r(r, pot%argv)
          ulk(i) = deutsch_long_k(k, pot%argv)
       end do

    ! case('HARD-SPHERE')
    !    do i = 1, plan%n
    !       r = i*plan%dr
    !       k = i*plan%dk
    !       ur(i) = hard_sphere_r(r, pot%argv)
    !       uk(i) = 0.0_dp
    !       usr(i) = ur(i)
    !       ulk(i) = 0.0_dp
    !    end do

    case('COULOMB-CORE')
       do i = 1, plan%n
          r = i*plan%dr
          k = i*plan%dk
          ur(i) = coulomb_core_r(r, pot%argv)
          uk(i) = coulomb_core_k(k, pot%argv)
          usr(i) = coulomb_core_short_r(r, pot%argv)
          ulk(i) = coulomb_core_long_k(k, pot%argv)
       end do

    case DEFAULT
       stat = POT_BAD_NAME
       write(*,*) 'Could not match ', trim(adjustl(pot%name)), ' to a potential.'
       goto 900

    end select

!!! Cleanup
900 continue

  end subroutine tabulate_potentials





  !!--------------------------------------------------------------------
  !! Coulomb
  !!   u(r) = A/r
  !!   u(k) = 4pi A/k^2
  !!   rolloff: erfc(Zr)
  !!--------------------------------------------------------------------

  pure function coulomb_r(r, argv) result(ur)
    real(dp) :: ur
    real(dp), intent(in) :: r, argv(1:coulomb_argc)
    real(dp) :: A
    A = argv(1)
    ur = A/r
  end function coulomb_r

  pure function coulomb_k(k, argv) result(uk)
    real(dp) :: uk
    real(dp), intent(in) :: k, argv(1:coulomb_argc)
    real(dp) :: A
    A = argv(1)
    uk = four_pi*A/k**2
  end function coulomb_k

  pure function coulomb_short_r(r, argv) result(usr)
    real(dp) :: usr
    real(dp), intent(in) :: r, argv(1:coulomb_argc)
    real(dp) :: A, Z
    A = argv(1)
    Z = argv(2)
    usr = A/r*erfc(Z*r)
  end function coulomb_short_r

  pure function coulomb_long_k(k, argv) result(ulk)
    real(dp) :: ulk
    real(dp), intent(in) :: k, argv(1:coulomb_argc)
    real(dp) :: A, Z
    A = argv(1)
    Z = argv(2)
    ulk = four_pi*A/k**2 *  exp(-(k/(2.0_dp*Z))**2)
  end function coulomb_long_k


  !!--------------------------------------------------------------------
  !! Yukawa
  !!   u(r) = A/r exp(-Br)
  !!   u(k) = 4pi A/(k^2 + B^2)
  !!   rolloff: exp(-Zr)
  !!--------------------------------------------------------------------

  pure function yukawa_r(r, argv) result(ur)
    real(dp) :: ur
    real(dp), intent(in) :: r, argv(1:yukawa_argc)
    real(dp) :: A, B
    A = argv(1)
    B = argv(2)
    ur = A/r*exp(-B*r)
  end function yukawa_r

  pure function yukawa_k(k, argv) result(uk)
    real(dp) :: uk
    real(dp), intent(in) :: k, argv(1:yukawa_argc)
    real(dp) :: A, B
    A = argv(1)
    B = argv(2)
    uk = four_pi*A * 1.0_dp/(k**2 + B**2) 
  end function yukawa_k

  pure function yukawa_short_r(r, argv) result(usr)
    real(dp) :: usr
    real(dp), intent(in) :: r, argv(1:yukawa_argc)
    real(dp) :: A, B, Z
    A = argv(1)
    B = argv(2)
    Z = argv(3)
    usr = A/r*exp(-(B+Z)*r)
  end function yukawa_short_r

  pure function yukawa_long_k(k, argv) result(ulk)
    real(dp) :: ulk
    real(dp), intent(in) :: k, argv(1:yukawa_argc)
    real(dp) :: A, B, Z
    A = argv(1)
    B = argv(2)
    Z = argv(3)
    ulk = four_pi*A * ( 1.0_dp/(k**2 + B**2) - 1.0_dp/(k**2 + (B+Z)**2) )
  end function yukawa_long_k


  !!--------------------------------------------------------------------
  !! Lennard-Jones
  !!   u(r) = A ( (B/r)^12 - (B/r)^6 )
  !!   u(k) = N/A
  !!   rolloff: N/A
  !!--------------------------------------------------------------------

  function lennard_jones_r(r, argv) result(ur)
    real(dp) :: ur
    real(dp), intent(in) :: r, argv(1:lennard_jones_argc)
    real(dp) :: A, B
    A = argv(1)
    B = argv(2)
    ur = A * (B/r)**6 * ( (B/r)**3 - 1.0_dp ) * ( (B/r)**3 + 1 )
  end function lennard_jones_r


  !!--------------------------------------------------------------------
  !! Induced Dipole + Hard Core
  !!   u(r) = A (B/r)^12 - C (D/r)^4
  !!   u(k) = N/A
  !!   rolloff: N/A
  !!--------------------------------------------------------------------

  function induced_dipole_r(r, argv) result(ur)
    real(dp) :: ur
    real(dp), intent(in) :: r, argv(1:induced_dipole_argc)
    real(dp) :: A, B, C, D
    A = argv(1)
    B = argv(2)
    C = argv(3)
    D = argv(4)
    ur = A * (B/r)**12  - C * (D/r)**4
  end function induced_dipole_r
  


  !!--------------------------------------------------------------------
  !! Hard Sphere
  !!   u(r) = oo if r < A, else 0
  !!   u(k) = N/A
  !!   rolloff: N/A
  !!--------------------------------------------------------------------

  ! function hard_sphere_r(r, argv) result(ur)
  !   real(dp) :: ur
  !   real(dp), intent(in) :: r, argv(1:hard_sphere_argc)
  !   real(dp) :: A
  !   A = argv(1)
  !   if (r < A) then
  !      ur = huge(r)
  !   else
  !      ur = 0.0_dp
  !   end if
  ! end function hard_sphere_r


  !!--------------------------------------------------------------------
  !! Deutsch
  !!   u(r) = A/r (1 - exp(-Br))
  !!   u(k) = 4pi A (1/k^2 - 1/(k^2 + B^2))
  !!   rolloff: exp(-Zr)
  !!--------------------------------------------------------------------

  function deutsch_r(r, argv) result(ur)
    real(dp) :: ur
    real(dp), intent(in) :: r, argv(1:deutsch_argc)
    real(dp) :: A, B
    A = argv(1)
    B = argv(2)
    ur = A/r * (1.0_dp - exp(-B*r))
  end function deutsch_r

  function deutsch_k(k, argv) result(uk)
    real(dp) :: uk
    real(dp), intent(in) :: k, argv(1:deutsch_argc)
    real(dp) :: A, B
    A = argv(1)
    B = argv(2)
    uk = four_pi*A/k**2 * B**2/(k**2 + B**2) 
  end function deutsch_k

  function deutsch_short_r(r, argv) result(usr)
    real(dp) :: usr
    real(dp), intent(in) :: r, argv(1:deutsch_argc)
    real(dp) :: A, B, Z
    A = argv(1)
    B = argv(2)
    Z = argv(3)
    usr = A/r * (1.0_dp - exp(-B*r))*exp(-Z*r)
  end function deutsch_short_r

  function deutsch_long_k(k, argv) result(ulk)
    real(dp) :: ulk
    real(dp), intent(in) :: k, argv(1:deutsch_argc)
    real(dp) :: A, B, Z
    A = argv(1)
    B = argv(2)
    Z = argv(3)
    ulk = four_pi*A * ( 1.0_dp/k**2 * B**2/(k**2 + B**2) &
         - 1.0_dp/(k**2 + Z**2) * B*(B + 2*Z)/(k**2 + (B + Z)**2) )
  end function deutsch_long_k


  !!--------------------------------------------------------------------
  !! Coulomb w/ Hard Core
  !!   u(r) = A/r + (B/r)**4
  !!   u(k) = N/A
  !!   rolloff: erfc(Zr)
  !!--------------------------------------------------------------------

  pure function coulomb_core_r(r, argv) result(ur)
    real(dp) :: ur
    real(dp), intent(in) :: r, argv(1:coulomb_core_argc)
    real(dp) :: A, B
    A = argv(1)
    B = argv(2)
    ur = A/r + (B/r)**4
  end function coulomb_core_r

  pure function coulomb_core_k(k, argv) result(uk)
    real(dp) :: uk
    real(dp), intent(in) :: k, argv(1:coulomb_core_argc)
    real(dp) :: A, B
    A = argv(1)
    B = argv(2)
    uk = four_pi*A/k**2
  end function coulomb_core_k

  pure function coulomb_core_short_r(r, argv) result(usr)
    real(dp) :: usr
    real(dp), intent(in) :: r, argv(1:coulomb_core_argc)
    real(dp) :: A, B, Z
    A = argv(1)
    B = argv(2)
    Z = argv(3)
    usr = A/r*erfc(Z*r) + (B/r)**4
  end function coulomb_core_short_r

  pure function coulomb_core_long_k(k, argv) result(ulk)
    real(dp) :: ulk
    real(dp), intent(in) :: k, argv(1:coulomb_core_argc)
    real(dp) :: A, B, Z
    A = argv(1)
    B = argv(2)
    Z = argv(3)
    ulk = four_pi*A/k**2 *  exp(-(k/(2.0_dp*Z))**2)
  end function coulomb_core_long_k

end module potentials
