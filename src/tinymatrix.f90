!!!
!!! TINYMATRIX -- Common operations on small matrices
!!!
module tinymatrix
  use kinds, only: dp
  implicit none
  
  private
  public :: det2, det3, inv2!, inv3

  ! interface for functions det() and inv() that dynamically dispatch
  ! to the correct data type and dimension?

contains
  
  pure function det2(a) result(det)
    real(dp) :: det
    real(dp), dimension(2,2), intent(in) :: a
    det = a(1,1)*a(2,2) - a(1,2)*a(2,1)
  end function det2

  pure function det3(a) result(det)
    real(dp) :: det
    real(dp), dimension(3,3), intent(in) :: a
    det =  a(1,1) * ( a(2,2)*a(3,3) - a(2,3)*a(3,2) ) &
         + a(1,2) * ( a(2,3)*a(3,1) - a(2,1)*a(3,3) ) &
         + a(1,3) * ( a(2,1)*a(3,2) - a(2,2)*a(3,1) )
  end function det3

  pure function inv2(a) result(inv)
    real(dp), dimension(2,2) :: inv
    real(dp), dimension(2,2), intent(in) :: a
    inv(1,1) = a(2,2)
    inv(1,2) = -a(1,2)
    inv(2,1) = -a(2,1)
    inv(2,2) = a(1,1)
    inv = inv/det2(a)
  end function inv2

!  pure function inv3(a) result(inv)
!    real(dp), dimension(3,3) :: inv
!    real(dp), dimension(3,3), intent(in) :: a
!    inv(1,1) = 0.0_dp
!    inv(1,2) = 0.0_dp
!    inv(1,3) = 0.0_dp
!    inv(2,1) = 0.0_dp
!    inv(2,2) = 0.0_dp
!    inv(2,3) = 0.0_dp
!    inv(3,1) = 0.0_dp
!    inv(3,2) = 0.0_dp
!    inv(3,3) = 0.0_dp
!    inv = inv/det3(a)
!  end function inv3
  
end module tinymatrix
