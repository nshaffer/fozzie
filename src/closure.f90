module closure
  use kinds, only: dp
  implicit none
  private
  public :: apply_closure

  integer, parameter :: CLOSURE_BAD_NAME = 1

contains

  subroutine apply_closure(name, ur, Nr, cr, br, stat)
    character(len=*) :: name
    real(dp), dimension(:), intent(in) :: ur, Nr
    real(dp), dimension(:), intent(out) :: cr, br
    integer, intent(out) :: stat
    
    stat = 0

    select case(name)
    case ('HNC')
       call hypernetted_chain(ur, Nr, cr, br)
    case ('PY')
       call percus_yevick(ur, Nr, cr, br)
    case DEFAULT
       stat = CLOSURE_BAD_NAME
       write(*,*) 'Could not match ', trim(adjustl(name)), ' to a closure.'
    end select
  end subroutine apply_closure

!!! ------ Closure implementations ------

  subroutine hypernetted_chain(ur, Nr, cr, br)
    real(dp), dimension(:), intent(in) :: ur, Nr
    real(dp), dimension(:), intent(out) :: cr, br
    br = 0.0_dp
    cr = exp(-ur + Nr + br) - (1.0_dp + Nr)
  end subroutine hypernetted_chain

  subroutine percus_yevick(ur, Nr, cr, br)
    real(dp), dimension(:), intent(in) :: ur, Nr
    real(dp), dimension(:), intent(out) :: cr, br
    br = log(1.0_dp + Nr) - Nr
    cr = exp(-ur + Nr + br) - (1.0_dp + Nr)
  end subroutine percus_yevick

end module closure
