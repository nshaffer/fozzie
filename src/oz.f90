module oz
  use kinds, only: dp
  use tinymatrix, only: inv2
  use radialft
  use species
  use closure
  use iteration_config
  implicit none
  private
  public :: oz_allocate_correlation_functions, oz_check_short_ranged, oz_iterate

  integer, parameter :: OZ_BAD_NAME         = 1
  integer, parameter :: OZ_BAD_NUM_SPECIES  = 2
  integer, parameter :: OZ_BAD_ALLOC        = 3
  integer, parameter :: OZ_NOT_SHORT_RANGED = 4
  integer, parameter :: OZ_NOT_CONVERGED    = -1

  real(dp), parameter :: OZ_RANGE_TOLERANCE = 1.0e-6_dp

  abstract interface
     subroutine solver_proc(species, plan, csr, ulk, Nsr, stat)
       import :: dp, species_t, rft_t
       type(species_t), dimension(:), intent(in) :: species
       type(rft_t), intent(inout) :: plan
       real(dp), dimension(size(species),size(species),plan%n), intent(in) :: csr, ulk
       real(dp), dimension(size(species),size(species),plan%n), intent(out) :: Nsr
       integer, intent(out) :: stat
     end subroutine solver_proc
  end interface


contains


  subroutine oz_iterate(iter_config, species, closure, plan, &
       usr, ulk, csr, Nsr, br, stat)
    type(iteration_config_t), intent(in) :: iter_config
    type(species_t), dimension(:), intent(in) :: species
    character(len=*), dimension(size(species),size(species)), intent(in) :: closure
    type(rft_t), intent(inout) :: plan
    real(dp), dimension(size(species),size(species),plan%n), intent(in) :: usr, ulk
    real(dp), dimension(size(species),size(species),plan%n), intent(inout) :: csr, Nsr, br
    integer, intent(out) :: stat

    procedure(solver_proc), pointer :: solver => NULL()
    real(dp), dimension(size(species),size(species),plan%n) :: csr_old
    real(dp) :: delta
    integer :: istep, is, js
    logical :: converged, beyond_iteration_limit

    stat = 0

    select case (trim(adjustl(iter_config%solver_name)))
    case ('NORMAL')
       write(*,*) 'Selecting NORMAL Ornstein-Zernike solver.'
       solver => oz_solve_normal
    case('SVT')
       write(*,*) 'Selecting SVT Ornstein-Zernike solver.'
       solver => oz_solve_svt
    case DEFAULT
       stat = OZ_BAD_NAME
       write(*,*) 'Unrecognized OZ solver: ', trim(adjustl(iter_config%solver_name))
       goto 900
    end select

    delta = 1.0_dp
    istep = 0
    is = 0; js = 0
    converged = .false.
    beyond_iteration_limit = .false.

    do while ( .not.(converged.or.beyond_iteration_limit) )
       csr_old = csr

       call solver(species, plan, csr, ulk, Nsr, stat)
       if ( stat /= 0 ) then
          write(*,*) 'Encountered an error in solver on iteration ', istep, '.'
          write(*,*) 'Aborting iteration.'
          goto 900
       end if

       do is = 1, size(species)
          do js = 1, size(species)
             call apply_closure(closure(is,js), usr(is,js,:), Nsr(is,js,:), csr(is,js,:), br(is,js,:), stat)
             if (stat /= 0) then
                write(*,*) 'Encountered an error in closure ', is, ',', js, 'on iteration ', istep, '.'
                write(*,*) 'Aborting iteration'
                goto 900
             end if
          end do
       end do

       csr = iter_config%mix*csr + (1.0_dp - iter_config%mix)*csr_old
       delta = plan%dr * sqrt(sum((csr-csr_old)**2)) / real(size(species)**2, kind=dp)

       converged = (delta < iter_config%tolerance)
       beyond_iteration_limit = (istep >= iter_config%max_num_iterations)
       istep = istep + 1

       write(*,*) istep, delta
    end do

    write(*,*) 'OZ iteration ended gracefully.'

    if ( beyond_iteration_limit .or. (.not. converged) ) then
       stat = OZ_NOT_CONVERGED
       write(*,*) 'Tolerance ', iter_config%tolerance, ' was not reached after ', istep, ' iterations.'
       goto 900
    end if

!!! Cleanup
900 continue

    if (associated(solver)) nullify(solver)

  end subroutine oz_iterate


  subroutine oz_solve_normal(species, plan, csr, ulk, Nsr, stat)
    type(species_t), dimension(:), intent(in) :: species
    type(rft_t), intent(inout) :: plan
    real(dp), dimension(size(species),size(species),plan%n), intent(in) :: csr, ulk
    real(dp), dimension(size(species),size(species),plan%n), intent(out) :: Nsr
    integer, intent(out) :: stat

    integer :: is, js, i
    real(dp), dimension(size(species),size(species),plan%n) :: ck, csk, Nsk, denom
    real(dp), dimension(plan%n) :: tmp
    real(dp), dimension(size(species),size(species)) :: rhorho

    stat = 0

    do is = 1, size(species)
       do js = is, size(species)
          rhorho(is,js) = sqrt(species(is)%density * species(js)%density)
          if (is /= js) rhorho(js,is) = rhorho(is,js)
       end do
    end do

    ! Transform cs(r) -> Cs(k)
    do is = 1, size(species)
       do js = is, size(species)
          tmp = csr(is,js,:)
          call rft(tmp, plan)
          csk(is,js,:) = tmp*rhorho(is,js)
          ck(is,js,:) = (tmp - ulk(is,js,:))*rhorho(is,js)
          if (is /= js) then
             csk(js,is,:) = csk(is,js,:)
             ck(js,is,:) = ck(is,js,:)
          end if
       end do
    end do


    ! Build I-C(k)
    do is = 1, size(species)
       do js = 1, size(species)
          if (is == js) then
             denom(is,js,:) = 1.0_dp - ck(is,js,:)
          else
             denom(is,js,:) = -ck(is,js,:)
          end if
       end do
    end do


    ! Invert I-C(k) and solve Ns(k) = inv(I-C).C - Cs
    ! TODO: check condition number and issue a warning if badly conditioned
    select case(size(species))
    case (1)
       Nsk = ck/denom - csk
    case (2)
       do i = 1, plan%n
          Nsk(:,:,i) = matmul(inv2(denom(:,:,i)), ck(:,:,i))
       end do
       Nsk = Nsk - csk
    case DEFAULT
       stat = OZ_BAD_NUM_SPECIES
       write(*,*) 'Only 1- and 2-species runs supported, got: ', size(species), '.'
       goto 900
    end select

    ! Transform Ns(k) -> Ns(r)
    do is = 1, size(species)
       do js = is, size(species)
          tmp = Nsk(is,js,:)
          call irft(tmp, plan)
          Nsr(is,js,:) = tmp/rhorho(is,js)
          if (is /= js) Nsr(js,is,:) = Nsr(is,js,:)
       end do
    end do

!!! Cleanup
900 continue


  end subroutine oz_solve_normal


  subroutine oz_solve_svt(species, plan, csr, ulk, Nsr, stat)
    ! Checked against old HNC code for SVT's cases -- Looks good!
    type(species_t), dimension(:), intent(in) :: species
    type(rft_t), intent(inout) :: plan
    real(dp), dimension(size(species),size(species),plan%n), intent(in) :: csr, ulk
    real(dp), dimension(size(species),size(species),plan%n), intent(out) :: Nsr
    integer, intent(out) :: stat

    integer :: is, js
    real(dp), dimension(size(species),size(species),plan%n) :: ck, csk, Nsk
    real(dp), dimension(plan%n) :: tmp, denom
    real(dp), dimension(size(species),size(species)) :: rhorho 
    real(dp) :: m1_over_m2, T1_over_T2, reduced_temperature, T12_over_sqrt_T1T2
    
    stat = 0

    if (size(species) /= 2) then
       stat = OZ_BAD_NUM_SPECIES
       write(*,*) 'SVT solver only supports exactly 2 species.'
       goto 900
    end if

    m1_over_m2 = species(1)%mass / species(2)%mass
    T1_over_T2 = species(1)%temperature / species(2)%temperature

    reduced_temperature = ( species(1)%mass * species(2)%temperature &
         + species(2)%mass * species(1)%temperature ) &
         / ( species(1)%mass + species(2)%mass )
    T12_over_sqrt_T1T2 = reduced_temperature &
         / sqrt( species(1)%temperature * species(2)%temperature )

    do is = 1, size(species)
       do js = is, size(species)
          rhorho(is,js) = sqrt(species(is)%density * species(js)%density)
          if (is /= js) rhorho(js,is) = rhorho(is,js)
       end do
    end do

    ! Transform cs(r) -> Cs(k)
    do is = 1, size(species)
       do js = is, size(species)
          tmp = csr(is,js,:)
          call rft(tmp, plan)
          csk(is,js,:) = tmp*rhorho(is,js)
          ck(is,js,:) = (tmp - ulk(is,js,:))*rhorho(is,js)
          if (is /= js) then
             csk(js,is,:) = csk(is,js,:)
             ck(js,is,:) = ck(is,js,:)
          end if
       end do
    end do

    ! Calulate each component of Ns(k) = inv(I-C).C - Cs
    denom = svt_denominator(ck, m1_over_m2, T1_over_T2, T12_over_sqrt_T1T2)
    Nsk(1,1,:) = svt_numerator_11(ck, m1_over_m2, T1_over_T2, T12_over_sqrt_T1T2) / denom
    Nsk(1,2,:) = svt_numerator_12(ck, m1_over_m2, T1_over_T2, T12_over_sqrt_T1T2) / denom
    Nsk(2,1,:) = Nsk(1,2,:)
    Nsk(2,2,:) = svt_numerator_22(ck, m1_over_m2, T1_over_T2, T12_over_sqrt_T1T2) / denom

    Nsk = Nsk - csk

    ! Transform Ns(k) -> Ns(r)
    do is = 1, size(species)
       do js = is, size(species)
          tmp = Nsk(is,js,:)
          call irft(tmp, plan)
          Nsr(is,js,:) = tmp/rhorho(is,js)
          if (is /= js) Nsr(js,is,:) = Nsr(is,js,:)
       end do
    end do
    
!!! Cleanup
900 continue
    

  end subroutine oz_solve_svt

  
  function svt_denominator(ck, mu, tau, f) result(denom)
    real(dp), dimension(:,:,:), intent(in) :: ck
    real(dp), intent(in) :: mu, tau, f
    real(dp), dimension(:), allocatable :: denom

    denom = &
         - ck(1,1,:)**2*ck(2,2,:)*tau &      
         + ck(1,1,:)**2*tau &                       
         + ck(1,1,:)*ck(1,2,:)**2*f**2*tau & 
         - ck(1,1,:)*ck(2,2,:)**2*mu &       
         + ck(1,1,:)*ck(2,2,:)*f*mu*sqrt(tau) & 
         + ck(1,1,:)*ck(2,2,:)*f*sqrt(tau) &    
         + ck(1,1,:)*ck(2,2,:)*mu &             
         + ck(1,1,:)*ck(2,2,:)*tau &            
         - ck(1,1,:)*f*mu*sqrt(tau) &                  
         - ck(1,1,:)*f*sqrt(tau) &                     
         - ck(1,1,:)*tau &                             
         + ck(1,2,:)**2*ck(2,2,:)*f**2*mu &  
         - ck(1,2,:)**2*f**2*mu &             
         - ck(1,2,:)**2*f**2*tau &            
         + ck(2,2,:)**2*mu &                        
         - ck(2,2,:)*f*mu*sqrt(tau) &                  
         - ck(2,2,:)*f*sqrt(tau) &                     
         - ck(2,2,:)*mu &                              
         + f*mu*sqrt(tau) &                                   
         + f*sqrt(tau)                                        

  end function svt_denominator

  function svt_numerator_11(ck, mu, tau, f) result(numer)
    real(dp), dimension(:,:,:), intent(in) :: ck
    real(dp), intent(in) :: mu, tau, f
    real(dp), dimension(:), allocatable :: numer

    numer = &                                            
           ck(1,1,:)**2*ck(2,2,:)*tau &         
         - ck(1,1,:)**2*tau &                          
         - ck(1,1,:)*ck(1,2,:)**2*f**2*tau &    
         + ck(1,1,:)*ck(2,2,:)**2*mu &                
         - ck(1,1,:)*ck(2,2,:)*f*mu*sqrt(tau) &          
         - ck(1,1,:)*ck(2,2,:)*f*sqrt(tau) &             
         - ck(1,1,:)*ck(2,2,:)*mu &                      
         + ck(1,1,:)*f*mu*sqrt(tau) &                           
         + ck(1,1,:)*f*sqrt(tau) &                              
         - ck(1,2,:)**2*ck(2,2,:)*f**2*mu &           
         + ck(1,2,:)**2*f**2*mu &                      
         + ck(1,2,:)**2*f**2                           

  end function svt_numerator_11

  function svt_numerator_12(ck, mu, tau, f) result(numer)
    real(dp), dimension(:,:,:), intent(in) :: ck
    real(dp), intent(in) :: mu, tau, f
    real(dp), dimension(:), allocatable :: numer

    numer = -ck(1,2,:) * f * sqrt(tau) &
         * (ck(1,1,:) + ck(2,2,:)*mu - mu - 1.0_dp)

  end function svt_numerator_12

  function svt_numerator_22(ck, mu, tau, f) result(numer)
    real(dp), dimension(:,:,:), intent(in) :: ck
    real(dp), intent(in) :: mu, tau, f
    real(dp), dimension(:), allocatable :: numer

    numer = &               
           ck(1,1,:)**2*ck(2,2,:)*tau &            
         - ck(1,1,:)*ck(1,2,:)**2*f**2*tau &       
         + ck(1,1,:)*ck(2,2,:)**2*mu &       
         - ck(1,1,:)*ck(2,2,:)*f*mu*sqrt(tau) &       
         - ck(1,1,:)*ck(2,2,:)*f*sqrt(tau) &          
         - ck(1,1,:)*ck(2,2,:)*tau &                  
         - ck(1,2,:)**2*ck(2,2,:)*f**2*mu &  
         + ck(1,2,:)**2*f**2*mu*tau &               
         + ck(1,2,:)**2*f**2*tau &                  
         - ck(2,2,:)**2*mu &                        
         + ck(2,2,:)*f*mu*sqrt(tau) &                        
         + ck(2,2,:)*f*sqrt(tau)                             

  end function svt_numerator_22

  
  subroutine oz_allocate_correlation_functions(num_species, num_gridpoints, csr, Nsr, br, stat)
    integer, intent(in) :: num_species, num_gridpoints
    real(dp), dimension(:,:,:), allocatable, intent(out) :: csr, Nsr, br
    integer, intent(out) :: stat

    stat = 0

    allocate( csr(num_species,num_species,num_gridpoints), stat=stat )
    if (stat /= 0) then
       stat = OZ_BAD_ALLOC
       write(*,*) 'Error while allocating direct correlation function.'
       goto 900
    else
       csr = 0.0_dp
    end if

    allocate( Nsr(num_species,num_species,num_gridpoints), stat=stat )
    if (stat /= 0) then
       stat = OZ_BAD_ALLOC
       write(*,*) 'Error while allocating nodal function.'
       goto 900
    else
       Nsr = 0.0_dp
    end if

    allocate( br(num_species,num_species,num_gridpoints), stat=stat )
    if (stat /= 0) then
       stat = OZ_BAD_ALLOC
       write(*,*) 'Error while allocating bridge function.'
       goto 900
    else
       br = 0.0_dp
    end if

!!! Cleanup
900 continue

    if (stat /= 0) then
       if (allocated(csr)) deallocate(csr)
       if (allocated(Nsr)) deallocate(Nsr)
    end if

  end subroutine oz_allocate_correlation_functions


  subroutine oz_check_short_ranged(csr, plan, stat)
    type(rft_t), intent(in) :: plan
    real(dp), dimension(plan%n), intent(in) :: csr
    integer, intent(out) :: stat

    if ( abs(csr(plan%n)) < OZ_RANGE_TOLERANCE ) then
       stat = 0
    else
       stat = OZ_NOT_SHORT_RANGED
       write(*,*) 'Direct correlation function is not short-ranged within absolute tolerance ', OZ_RANGE_TOLERANCE 
       write(*,*) 'At R_max=', plan%n*plan%dr, ', C_s(r)=', csr(plan%n)
       write(*,'(a,/,3(t4,a,/))') 'Consider', &
            '1) increasing the number of gridpoints', &
            '2) decreasing the grid spacing', &
            '3) decreasing the potential rolloff length, if applicable'
    end if

  end subroutine oz_check_short_ranged

end module oz

