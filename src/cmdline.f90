module cmdline
  implicit none

  public :: parse_command_line_arguments
  public :: CMD_ARG_MAX_LEN

  integer, parameter :: CMD_ARG_MAX_LEN = 32

  integer, parameter :: BAD_OPT_NAME = 1

contains

  subroutine parse_command_line_arguments(config_fname, guess_fname, output_prefix, stat)
    character(len=CMD_ARG_MAX_LEN), intent(out) :: config_fname
    character(len=CMD_ARG_MAX_LEN), intent(out) :: guess_fname
    character(len=CMD_ARG_MAX_LEN), intent(out) :: output_prefix
    integer, intent(out) :: stat

    character(len=CMD_ARG_MAX_LEN) :: opt, val
    integer :: iarg

    stat = 0

    config_fname = ''
    guess_fname = ''
    output_prefix = ''

    iarg = 1 ! skip iarg=0, which is the program name

    argloop: do
       call get_command_argument(iarg, opt)

       select case(trim(adjustl(opt)))

       case('-h', '--help')
          call print_help()
          stop

       case('-C', '--config-file')
          iarg = iarg+1
          call get_command_argument(iarg, val)
          config_fname = trim(adjustl(val))

       case('-G', '--guess-file')
          iarg = iarg+1
          call get_command_argument(iarg, val)
          guess_fname = trim(adjustl(val))

       case('-P', '--prefix')
          iarg = iarg+1
          call get_command_argument(iarg, val)
          output_prefix = trim(adjustl(val))

       case('')
          exit argloop

       case DEFAULT
          stat = BAD_OPT_NAME
          write(*,*) 'Unrecognized command-line option: '//trim(adjustl(opt))
          call print_help()
          return

       end select

       iarg = iarg+1
    end do argloop

  end subroutine parse_command_line_arguments


  subroutine print_help()

    write(*,'(t1,a)') 'Usage: foz [options]'
    write(*,'(t1,a)') 'Options:'
    write(*,'(t3,a,t40,a)') '-C FILE, --config-file FILE', 'Read run configuration from FILE.'
    write(*,'(t3,a,t40,a)') '-G FILE, --guess-file FILE', 'Read initial guess data from FILE.'
    write(*,'(t3,a,t40,a)') '-P STR, --prefix STR', 'Prefix to use on output data files.'
    write(*,'(t3,a,t40,a)') '-h, --help', 'Print this message and exit.'

  end subroutine print_help

end module cmdline
