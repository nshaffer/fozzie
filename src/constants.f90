module constants
  use kinds, only: dp
  implicit none

    character(len=36), parameter :: hrule = "------------------------------------"
  
  real(dp), parameter :: zero = 0.0_dp
  real(dp), parameter :: one = 1.0_dp
  real(dp), parameter :: two = 2.0_dp
  real(dp), parameter :: three = 3.0_dp
  real(dp), parameter :: four = 4.0_dp
  real(dp), parameter :: five = 5.0_dp
  real(dp), parameter :: six = 6.0_dp
  real(dp), parameter :: seven = 7.0_dp
  real(dp), parameter :: eight = 8.0_dp
  real(dp), parameter :: nine = 9.0_dp
  real(dp), parameter :: ten = 10.0_dp

  real(dp), parameter :: one_half = one/two
  real(dp), parameter :: one_third = one/three
  real(dp), parameter :: two_thirds = two/three
  real(dp), parameter :: one_fourth = one/four
  real(dp), parameter :: three_fourths = three/four
  real(dp), parameter :: two_fifths = two/five
  real(dp), parameter :: three_fifths = three/five
  real(dp), parameter :: four_fifths = four/five
  
  real(dp), parameter :: three_halves = three/two
  real(dp), parameter :: four_thirds = four/three
  real(dp), parameter :: five_halves = five/two
  real(dp), parameter :: five_thirds = five/three
  real(dp), parameter :: five_fourths = five/four

  real(dp), parameter :: root_two = two**one_half
  real(dp), parameter :: root_three = three**one_half
  real(dp), parameter :: cbrt_two = two**one_third
  real(dp), parameter :: cbrt_three = three**one_third
  
  real(dp), parameter :: pi = 3.14159265358979_dp
  real(dp), parameter :: two_pi = two*pi
  real(dp), parameter :: three_pi = three*pi
  real(dp), parameter :: four_pi = four*pi
  real(dp), parameter :: root_pi = pi
  
end module constants
