program main
  use kinds, only: dp
  use cmdline
  use input
  use radialft
  use oz
  use potentials
  use closure
  use species
  use iteration_config
  implicit none

  character(len=CMD_ARG_MAX_LEN) :: config_fname, guess_fname, output_prefix

  type(iteration_config_t) :: iter_config
  type(rft_t) :: plan
  type(species_t), dimension(:), allocatable :: species
  type(potential_t), dimension(:,:), allocatable :: pots
  character(len=CLOSURE_NAME_LEN), dimension(:,:), allocatable :: closures
  real(dp), dimension(:,:,:), allocatable :: ur, uk, usr, ulk, csr_in, csr, Nsr, br
  integer :: is, js
  logical :: exists
  integer :: stat

  stat = 0

  call parse_command_line_arguments(config_fname, guess_fname, output_prefix, stat)
  if (stat /= 0) then
     write(*,*) 'Error while parsing command line.'
     goto 900
  end if

  call write_header()

  call read_config( config_fname, iter_config, plan, species, pots, closures, stat )
  if (stat /= 0) then
     write(*,*) 'Error while reading input.'
     goto 900
  else
     write(*,*) 'Input complete.'
  end if

  
  write(*,*) 'Setting up potential arrays.'
  call allocate_potential_arrays( size(species), plan%n, ur, uk, usr, ulk, stat )
  do is = 1, size(species)
     do js = 1, size(species)
        call tabulate_potentials( pots(is,js), plan, ur(is,js,:), uk(is,js,:), usr(is,js,:), ulk(is,js,:), stat )
        if (stat /= 0) then
           write(*,*) 'Error while setting up potential arrays.'
           goto 900
        else
           write(*,*) 'Potential setup complete.'
        end if
     end do
  end do

  write(*,*) 'Setting up correlation functions.'
  call oz_allocate_correlation_functions( size(species), plan%n, csr, Nsr, br, stat )

  if ( len_trim(adjustl(guess_fname)) == 0 ) then
     csr = exp(-usr) - 1.0_dp
  else
     inquire(file=guess_fname, exist=exists)
     if (.not.exists) then
        write(*,*) 'Guess file "'//trim(adjustl(guess_fname))//'" does not exist.'
        goto 900
     else
        call load_pair_function(guess_fname, size(species), plan%n, csr)
     end if
  end if

  Nsr = 0.0_dp
  br = 0.0_dp

  do is = 1, size(species)
     do js = 1, size(species)
        call oz_check_short_ranged( csr(is,js,:), plan, stat )
        if (stat /= 0) then
           write(*,*) 'Error while setting up correlation function ', is, js
           goto 900
        else
           write(*,*) 'Correlation function setup complete.'
        end if
     end do
  end do

  write(*,*) 'Begin iteration.'
  call oz_iterate( iter_config, species, closures, plan, usr, ulk, csr, Nsr, br, stat )
  if ( stat /= 0 ) then
     write(*,*) 'Error during iteration.'
     goto 900
  else
     write(*,*) 'Finished iteration.'
  end if

  write(*,*) 'Writing results.'
  call write_output(species, plan, ur, uk, usr, ulk, csr, Nsr, br, output_prefix)
  write(*,*) 'Finished writing.'
    

!!! Cleanup
900 continue

  write(*,*) 'Cleaning up resources.'

  if (allocated(br)) deallocate(br)
  if (allocated(Nsr)) deallocate(Nsr)
  if (allocated(csr)) deallocate(csr)
  if (allocated(csr_in)) deallocate(csr_in)
  if (allocated(ulk)) deallocate(ulk)
  if (allocated(usr)) deallocate(usr)
  if (allocated(uk)) deallocate(uk)
  if (allocated(ur)) deallocate(ur)

  if (allocated(closures)) deallocate(closures)

  if (allocated(pots)) then
     do is = 1, size(species)
        do js = 1, size(species)
           call final_potential(pots(is,js))
        end do
     end do
  end if

  if (allocated(species)) deallocate(species)

  call rft_final(plan)

  write(*,*) 'Done.'
  write(*,*)

  if (stat /= 0) then
     write(*,*) 'Finished with status ', stat
     stop 1
  end if


contains

  subroutine write_header()
    use, intrinsic :: iso_fortran_env, only: compiler_version
    
    character(len=8) :: date
    character(len=10) :: time
    character(len=5) :: zone
    character(len=80) :: timestamp
    call date_and_time(date, time, zone)
    write(timestamp, '(a4,"/",a2,"/",a2," ",a2,":",a2,":",a2," UTC",a5)') &
         date(1:4), date(5:6), date(7:8), time(1:2), time(2:3), time(4:5), zone
    
    write(*,*) '------------------------------------------------------------------------'
    write(*,*) 'Starting FOZZIE'
    write(*,*) 'Compiler: ', compiler_version()
    write(*,*) 'Timestamp: ', trim(adjustl(timestamp))
    write(*,*) '------------------------------------------------------------------------'
    
  end subroutine write_header


  subroutine save_pair_function(fname, num_species, num_gridpts, dx, pairfun)
    character(len=*), intent(in) :: fname
    integer, intent(in) :: num_species, num_gridpts
    real(dp), intent(in) :: dx
    real(dp), dimension(num_species,num_species,num_gridpts), intent(in) :: pairfun

    integer :: u, ix, is, js, irec
    real(dp), dimension(:), allocatable :: record

    ! record = [ x, f11, ... , f1s, f21, ... , f2s, ... , fs1, ..., fss ]
    allocate( record(1+num_species**2) )

    open(newunit=u, file=trim(fname))
    do ix = 1, num_gridpts
       record = 1.0e-300_dp
       irec = 1
       record(irec) = ix*dx
       do is = 1, num_species
          do js = 1, num_species
             irec = irec + 1
             record(irec) = pairfun(is,js,ix)
          end do
       end do
       write(u,*) record
    end do
    close(u)

    if (allocated(record)) deallocate(record)

    write(*,*) 'Wrote to ', fname
  end subroutine save_pair_function


  subroutine load_pair_function(fname, num_species, num_gridpts, pairfun)
    character(len=*), intent(in) :: fname
    integer, intent(in) :: num_species, num_gridpts
    real(dp), dimension(num_species,num_species,num_gridpts), intent(inout) :: pairfun

    integer :: u, ix, is, js, irec
    real(dp), dimension(:), allocatable :: record

    ! record = [ x, f11, ... , f1s, f21, ... , f2s, ... , fs1, ..., fss ]
    allocate( record(1+num_species**2) )

    open(newunit=u, file=trim(fname))
    do ix = 1, num_gridpts
       record = -1.0e300_dp
       read(u,*) record
       irec = 1
       do is = 1, num_species
          do js = 1, num_species
             irec = irec + 1
             pairfun(is,js,ix) = record(irec)
          end do
       end do
    end do
    close(u)

    if (allocated(record)) deallocate(record)
  end subroutine load_pair_function


  subroutine write_output(species, plan, ur, uk, usr, ulk, csr, Nsr, br, prefix)
    type(species_t), dimension(:), intent(in) :: species
    type(rft_t), intent(inout) :: plan
    real(dp), dimension(size(species), size(species), plan%n), intent(in) :: & 
         ur, uk, usr, ulk, csr, Nsr, br
    character(len=*), intent(in) :: prefix

    real(dp), dimension(size(species), size(species), plan%n) :: &
         csk, Nsk, gr, wr, ck, Sk
    
    integer :: is, js

    csk = -1.0e300_dp
    Nsk = -1.0e300_dp
    gr = -1.0e300_dp
    ck = -1.0e300_dp
    Sk = -1.0e300_dp
    
    csk = csr
    Nsk = Nsr
    do is = 1, size(species)
       do js = 1, size(species)
          call rft(csk(is,js,:), plan)
          call rft(Nsk(is,js,:), plan)
       end do
    end do
    
    ! Direct correlation function
    ck = csk - ulk

    ! Static structure factor
    do is = 1, size(species)
       do js = 1, size(species)
          Sk(is,js,:) = sqrt( species(is)%density*species(js)%density ) * (Nsk(is,js,:) + csk(is,js,:))
          if (is == js) Sk(is,js,:) = 1.0_dp + Sk(is,js,:) 
       end do
    end do

    ! Pair distrubution function
    gr = Nsr + csr + 1.0_dp

    ! Potential of mean force
    wr = usr - Nsr - br

    call save_pair_function( trim(adjustl(prefix))//'ur.dat', size(species), plan%n, plan%dr, ur )
    call save_pair_function( trim(adjustl(prefix))//'gr.dat', size(species), plan%n, plan%dr, gr )
    call save_pair_function( trim(adjustl(prefix))//'wr.dat', size(species), plan%n, plan%dr, wr )
    call save_pair_function( trim(adjustl(prefix))//'br.dat', size(species), plan%n, plan%dr, br )
    call save_pair_function( trim(adjustl(prefix))//'csr.dat', size(species), plan%n, plan%dr, csr )

    call save_pair_function( trim(adjustl(prefix))//'uk.dat', size(species), plan%n, plan%dk, uk )
    call save_pair_function( trim(adjustl(prefix))//'ck.dat', size(species), plan%n, plan%dk, ck )
    call save_pair_function( trim(adjustl(prefix))//'Sk.dat', size(species), plan%n, plan%dk, Sk )

  end subroutine write_output

end program main
