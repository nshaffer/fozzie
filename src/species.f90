module species
  use kinds, only: dp
  use constants, only: four_pi
  implicit none
  private
  public :: species_t, init_species

  real(dp), parameter :: DUMMY_DP = -1.0e300_dp

  real(dp), parameter :: elementary_charge = 1.602176620898e-19_dp ! [C]
  real(dp), parameter :: vacuum_permittivity = 8.854187817e-12_dp ! [C^2/(N.m)]
  real(dp), parameter :: boltzmann_constant = 1.3806485279e-23_dp ! [J/K]
  real(dp), parameter :: reduced_planck_constant = 1.05457180013e-34_dp ! [J.s]
  real(dp), parameter :: atomic_mass_unit = 1.66053904020e-27_dp ! [kg]

  integer, parameter :: SPECIES_OK = 0
  integer, parameter :: SPECIES_BAD_VALUE = 1

  type species_t
     real(dp) :: charge = DUMMY_DP      ! [e]
     real(dp) :: mass = DUMMY_DP        ! [u]
     real(dp) :: density = DUMMY_DP     ! [m^-3]
     real(dp) :: temperature = DUMMY_DP ! [K]
  end type species_t

contains


  subroutine init_species(species, charge, mass, density, temperature, stat)
    type(species_t), intent(out) :: species
    real(dp), intent(in) :: charge
    real(dp), intent(in) :: mass
    real(dp), intent(in) :: density
    real(dp), intent(in) :: temperature
    integer, intent(out) :: stat

    stat = SPECIES_OK

    species%charge = charge
    species%mass = mass
    species%density = density
    species%temperature = temperature

    call check_species(species, stat)

  end subroutine init_species


  subroutine check_species(species, stat)
    type(species_t), intent(in) :: species
    integer, intent(out) :: stat

    stat = SPECIES_OK

    if ( species%mass <= 0.0_dp ) then
       stat = SPECIES_BAD_VALUE
       write(*,*) 'Species mass must be positive, got: ', species%mass

    else if ( species%density <= 0.0_dp ) then
       stat = SPECIES_BAD_VALUE
       write(*,*) 'Species density must be positive, got: ', species%density

    else if ( species%temperature <= 0.0_dp  ) then
       stat = SPECIES_BAD_VALUE
       write(*,*) 'Species temperature must be positive, got: ', species%temperature
    end if

  end subroutine check_species

end module species


  ! pure function reduced_mass(s1, s2)
  !   !!
  !   !! Compute the reduced mass of two species,
  !   !!
  !   !!   m_{ij} = m_i m_j / (m_i + m_j)
  !   !!
  !   type(species_t), intent(in) :: s1, s2
  !   real(dp) :: reduced_mass

  !   reduced_mass = ( s1%mass * s2%mass ) / ( s1%mass + s1%mass )
  ! end function reduced_mass


  ! pure function reduced_temperature(s1, s2)
  !   !!
  !   !! Compute the reduced (mass-weighted) temperature of two species,
  !   !!
  !   !!   T_{ij} = m_{ij} ( T_i/m_i + T_j/m_j )
  !   !!
  !   type(species_t), intent(in) :: s1, s2
  !   real(dp) :: reduced_temperature

  !   reduced_temperature = reduced_mass(s1, s2) &
  !        * ( s1%temperature / s1%mass + s2%temperature / s2%mass )
  ! end function reduced_temperature


  ! pure function interparticle_spacing(species)
  !   !!
  !   !! Compute the mean distance between particles in a system of
  !   !! several species, defined as
  !   !!
  !   !!   a = ( 4\pi n/3 )^(-1/3)
  !   !!
  !   !! where `n` is the total number of particles per unit volume.
  !   !!
  !   type(species_t), dimension(:), intent(in) :: species
  !   real(dp) :: interparticle_spacing

  !   interparticle_spacing = ( (3.0_dp/four_pi)*sum(species%density) )**(1.0_dp/3.0_dp)
  ! end function interparticle_spacing


  ! pure function coupling_strengths(species)
  !   !!
  !   !! Compute the Coulomb coupling strength of all pairwise
  !   !! combinations of an array of species based on the definition
  !   !!
  !   !!                          Z_i Z_j e^2
  !   !!   \Gamma_{ij} = -----------------------------
  !   !!                  4\pi\epsilon_0 a k_B T_{ij}
  !   !!
  !   !! where `a` is the system interparticle spacing and `T_{ij}` is
  !   !! the mass-weighted reduced temperature.
  !   !!
  !   type(species_t), dimension(:), intent(in) :: species
  !   real(dp), dimension(size(species), size(species)) :: coupling_strengths

  !   integer :: is, js

  !   coupling_strengths = DUMMY_DP
  !   do is = 1, size(species)
  !      do js = 1, size(species)
  !         coupling_strengths(is,js) = &
  !              ( elementary_charge / (four_pi*vacuum_permittivity) ) &
  !              * ( elementary_charge / boltzmann_constant ) &
  !              * ( species(is)%charge * species(js)%charge ) &
  !              / interparticle_spacing(species) &
  !              / reduced_temperature(species(is), species(js))
  !      end do
  !   end do
  ! end function coupling_strengths


  ! pure elemental function debye_length(s)
  !   !!
  !   !! Compute the Debye length of a species, defined by
  !   !!
  !   !!                           4\pi Z_i^2 e^2 n_i
  !   !!   \lambda_{Di} =  \sqrt( -------------------- )
  !   !!                                 k_B T_i
  !   !!
  !   type(species_t), intent(in) :: s
  !   real(dp) :: debye_length

  !   debye_length = &
  !        (boltzmann_constant/elementary_charge) &
  !        / (four_pi*elementary_charge) &
  !        * s%charge**2 * s%density / s%temperature
  !   debye_length = sqrt(debye_length)
  ! end function debye_length


  ! pure function reduced_thermal_wavelength(s1, s2)
  !   !!
  !   !! Compute the reduced thermal de Broglie wavelength of a species
  !   !! pair, defined by
  !   !!
  !   !!   \Lambda_{ij} = \hbar / \sqrt( 4\pi m_{ij} k_B T_{ij} )
  !   !!
  !   !! so that when i=j, the result is the the ordinary thermal
  !   !! wavelength,
  !   !!
  !   !!   \Lambda_{ii} = \Lambda_i = \hbar / \sqrt( 2\pi m_i k_B T_i )
  !   !!
  !   type(species_t), intent(in) :: s1, s2
  !   real(dp) :: reduced_thermal_wavelength

  !   reduced_thermal_wavelength = reduced_planck_constant / sqrt(four_pi) &
  !        / sqrt(boltzmann_constant) / sqrt(atomic_mass_unit) &
  !        * sqrt(reduced_mass(s1, s2) * reduced_temperature(s1, s2))
  ! end function reduced_thermal_wavelength


