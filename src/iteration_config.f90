module iteration_config
  use kinds, only: dp
  implicit none
  private
  public :: iteration_config_t, init_iteration_config, SOLVER_NAME_LEN

  integer, parameter :: SOLVER_NAME_LEN = 8

  type :: iteration_config_t
     character(len=SOLVER_NAME_LEN) :: solver_name
     integer :: max_num_iterations = -1
     real(dp) :: tolerance = -1.0_dp
     real(dp) :: mix = -1.0_dp
  end type iteration_config_t

  integer, parameter :: ITER_OK                     = 0
  integer, parameter :: ITER_BAD_MAX_NUM_ITERATIONS = 1
  integer, parameter :: ITER_BAD_TOLERANCE          = 2
  integer, parameter :: ITER_BAD_MIX                = 3

contains

  subroutine init_iteration_config(iter, solver_name, max_num_iterations, tolerance, mix, stat)
    type(iteration_config_t), intent(inout) :: iter
    integer, intent(in) :: max_num_iterations
    character(len=*), intent(in) :: solver_name
    real(dp), intent(in) :: tolerance
    real(dp), intent(in) :: mix
    integer, intent(out) :: stat

    stat = ITER_OK

    iter%solver_name = solver_name
    iter%max_num_iterations = max_num_iterations
    iter%tolerance = tolerance
    iter%mix = mix

    call check_iteration_config(iter, stat)

  end subroutine init_iteration_config

  subroutine check_iteration_config(iter, stat)
    type(iteration_config_t), intent(in) :: iter
    integer, intent(out) :: stat

    stat = ITER_OK

    if ( iter%max_num_iterations < 1 ) then
       stat = ITER_BAD_MAX_NUM_ITERATIONS
       write(*,*) 'Maximum number of iterations must be at least 1, is: ', iter%max_num_iterations, '.'

    else if ( iter%tolerance <= 0.0_dp ) then
       stat = ITER_BAD_TOLERANCE
       write(*,*) 'Self-consistency tolerance must be positive, is: ', iter%tolerance, '.'

    else if ( (iter%mix <= 0.0_dp) .or. (iter%mix > 1.0_dp) ) then
       stat = ITER_BAD_MIX
       write(*,*) 'Mixing parameter must be positive and less than 1, is: ', iter%mix, '.'
    end if

  end subroutine check_iteration_config

end module iteration_config
