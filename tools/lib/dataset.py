#!/usr/bin/env python
import os
import numpy as np


__all__ = ['Dataset']


class Dataset(object):
    """A class to dynamically load a `fozzie` dataset.

    Analyses of `fozzie` results often requires multiple output
    quantities (e.g., both `g(r)` and `S(k)`) and/or comparisons
    across many different runs. For these cases, it is useful to
    selectively load the subset of results needed, especially when
    working with calculations performed on a fine `r`/`k` grid
    and/or multi-species runs. At a very basic level, the Dataset
    class is meant to replace code like the following
    
    >>> import numpy as np
    >>> ocp_gr = np.genfromtxt('./ocp/gr.dat', names=['r','11'])
    >>> ocp_Sk = np.genfromtxt('./ocp/Sk.dat', names=['k','11'])
    >>> tcp_gr = np.genfromtxt('./tcp/gr.dat', names=['r','11','12','21','22'])
    >>> tcp_Sk = np.genfromtxt('./tcp/Sk.dat', names=['k','11','12','21','22'])
    >>> result = some_gross_analysis(ocp_gr, ocp_Sk, tcp_gr, tcp_Sk)

    with code like the following

    >>> from fozziedata import Dataset
    >>> ocp = Dataset('./ocp/')
    >>> tcp = Dataset('./tcp/')
    >>> result = some_less_gross_analysis(ocp, tcp)

    While the second example happens to be more concise, the main
    upshot is that the function `some_less_gross_analysis` 
    operates on Datasets. The details of which correlation
    functions to load from each Dataset happens internally, and
    whatever data is no longer needed after processing can also
    be freed internally.

    If we know that we'll want to keep the data around in memory
    for a little while, we can load it from the get-go
    
    >>> ocp = Dataset('./ocp/')
    >>> ocp.load('r', 'k', 'gr', 'Sk')
    >>> result = some_analysis(ocp.r, ocp.gr, ocp.k, ocp.Sk)
    >>> # ... some more processing ...
    >>> ocp.unload('r', 'k', 'gr', 'Sk')

    Parameters
    ----------
    path : str
        Absolute path to the directory holding the output data 
        files.
    prefix : str, optional
        Prefix attached to each data file name. If `fozzie` was 
        run with the `-P STR` (`--prefix STR`) flags, then this
        should most likely one should construct the Dataset with
        `prefix=STR`. Default is `''`, i.e., no prefix.
            
    Attributes
    ----------
    keys : set of str
        A set of abbreviations for data files. By default, it is
        `{'ur', 'gr', 'wr', 'br', 'uk', 'Sk', 'ck'}`. Changing
        its value will likely lead to unintended behavior.
    files : dict
        A mapping between the data abbreviations (`Dataset.keys`)
        and the names of files in the dataset corresponding to 
        each. If no file is present (because it was deleted to
        conserve disk space, say), then it is associated with the
        value `None`.
    pair_labels : list of str
        A list of strings like `'11'` or `'32'` for indexing into
        data arrays, e.g., `ds.gr['11']`. 

    Methods
    -------
    load(*args)
        Load data arrays into memory and provide attribute ('dot')
        access to them.
    unload(*args)
        Delete data arrays from memory and remove attribute access
        to them.
    """
    
    keys = {'ur', 'gr', 'wr', 'br', 'uk', 'Sk', 'ck'}

    
    def __init__(self, path, prefix=''):
        assert os.path.isdir(path), "Directory %s does not exist."%path
        self.files = dict()
        for key in Dataset.keys:
            val = os.path.join(path, prefix + key + '.dat') 
            self.files[key] = val if os.path.isfile(val) else None
        self.pair_labels = self._make_pair_labels()

        
    def _make_pair_labels(self):
        some_file = filter(lambda _: _ is not None, self.files.values())[0]
        with open(some_file, 'r') as f:
            n_columns = len(f.readline().strip().split())
        n_species = int((n_columns-1)**0.5)
        return ['%d%d' % (i+1, j+1) for i in xrange(n_species)
                                    for j in xrange(n_species)]

    
    def load(self, *args):
        """Load data arrays into memory and provide attribute
        ('dot') access to them.
        """
        for key in args:
            assert key in Dataset.keys | {'k', 'r'}, \
                "'%s' is not a recognized data key."%key
            if key == 'k':
                some_kfile = filter(lambda _: str(_).endswith('k.dat'),
                                    self.files.values())[0]
                arr = np.loadtxt(some_kfile, usecols=(0,))
            elif key == 'r':
                some_rfile = filter(lambda _: str(_).endswith('r.dat'),
                                    self.files.values())[0]
                arr = np.loadtxt(some_rfile, usecols=(0,))
            else:
                arr = np.genfromtxt(strip_first_column(self.files[key]),
                                    names=self.pair_labels)
            self.__dict__[key] = arr 

        
    def unload(self, *args):
        """Delete data arrays from memory and remove attribute 
        access to them.
        """
        for key in args:
            assert key in Dataset.keys | {'k', 'r'}, \
                "'%s' is not a recognized data key."%key
            del self.__dict__[key]


def strip_first_column(fname, delimiter=None):
    with open(fname, 'r') as f:
        for line in f:
            try:
                first, rest = line.split(delimiter, 1)
                yield rest
            except IndexError:
                continue
