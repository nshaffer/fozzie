#!/usr/bin/env python
from collections import namedtuple
import numpy as np


Species = namedtuple('Species', ['charge', 'mass', 'conc', 'temperature'])


class CoulombPotential(object):
    def __init__(self, strength, rolloff):
        self.name = 'COULOMB'
        self.argv = [float(x) for x in (strength, rolloff)]
        
class YukawaPotential(object):
    def __init__(self, strength, screen, rolloff):
        self.name = 'YUKAWA'
        self.argv = [float(x) for x in (strength, screen, rolloff)]

class DeutschPotential(object):
    def __init__(self, strength, soft_length, rolloff):
        self.name = 'DEUTSCH'
        self.argv = [float(x) for x in (strength, soft_length, rolloff)]

class CoulombCorePotential(object):
    def __init__(self, strength, diameter, rolloff):
        self.name = 'COULOMB-CORE'
        self.argv = [float(x) for x in (strength, diameter, rolloff)]

class HardSpherePotential(object):
    def __init__(self, diameter):
        self.name = 'HARD-SPHERE'
        self.argv = [float(x) for x in (diameter,)]

class LennardJonesPotential(object):
    def __init__(self, strength, diameter):
        self.name = 'LJ6-12'
        self.argv = [float(x) for x in (strength, diameter)]
    

class FozzieConfiguration(object):
    """Contains parameters to build Fozzie input files.
    
    Attributes
    ----------
    solver : str
        Name of the Ornstein-Zernike solution method.

    max_iterations : int
        Maximum number of iterations to use in OZ solver.

    tolerance : float
        Self-consistence threshold to determine when the 
        solution has converged.

    mix : float
        Fraction of new and old iterations used to update
        OZ trial solutions.

    num_gridpoints : int
        Number of grid points used in the solution domain.
    
    dr : float
        Distance between two grid points in real space.

    species : sequence of `Species` objects
        See `Species` details.

    potentials : 2d sequence of `Potential` objects
        Grid of interaction potentials for each possible
        pairing of species. 
    
    closures : 2d sequence of str
        Grid of names of closure relations. 
    """
    supported_solvers = ['NORMAL', 'SVT']
    supported_potentials = [CoulombPotential,
                            YukawaPotential,
                            DeutschPotential,
                            CoulombCorePotential,
                            HardSpherePotential,
                            LennardJonesPotential]
    supported_closures = ['HNC', 'PY']

    def __init__(self, 
                 solver, max_iterations, tolerance, mix,
                 num_gridpoints, dr,
                 species, potentials, closures):
        
        self.solver = str(solver).upper()
        self.max_iterations = int(max_iterations)
        self.tolerance = float(tolerance)
        self.mix = float(mix)
        self.num_gridpoints = int(num_gridpoints)
        self.dr = float(dr)
        self.species = species
        self.potentials = potentials
        self.closures = closures

        self._validate()

    def _validate(self):
        assert self.solver in FozzieConfiguration.supported_solvers
        for pp in self.potentials:
            for p in pp:
                assert type(p) in FozzieConfiguration.supported_potentials, \
                    'Unsupported potential model: {:s}'.format(type(p))
        for cc in self.closures:
            for c in cc:
                assert c in FozzieConfiguration.supported_closures, \
                    'Unsupported closure model: {:s}'.format(c)
        assert np.shape(self.potentials) == (len(self.species),)*2
        assert np.shape(self.closures) == np.shape(self.potentials)

    def write_to(self, fname):
        """Create a formatted ``fozzie`` config file."""
        from textwrap import dedent

        iteration_block = """
        --ITERATION
        {:s}\tsolver
        {:d}\tmax iterations
        {:g}\ttolerance
        {:g}\tmix
        """.format(self.solver, self.max_iterations, self.tolerance, self.mix)
        iteration_block = dedent(iteration_block)
        # remove spurious blank line above "ITERATION"
        iteration_block = iteration_block.lstrip('\n')

        plan_block = """
        --PLAN
        {:d}\tgrid_points
        {:g}\tdr [a]
        """.format(self.num_gridpoints, self.dr)
        plan_block = dedent(plan_block)

        species_block = """
        --SPECIES
        {:d} species
        i\tcharge\tmass\tconc\ttemperature
        """.format(len(self.species))
        species_block = dedent(species_block)
        for i, s in enumerate(self.species):
            species_block += '{:d}\t{:g}\t{:g}\t{:g}\t{:g}\n'.format(i+1, *s)
        
        potentials_block = """
        --POTENTIALS
        i j name\t\targc\targv
        """
        potentials_block = dedent(potentials_block)
        for i, pp in enumerate(self.potentials):
            for j, p in enumerate(pp):
                potentials_block += '{:d} {:d} {:s}\t\t{:d}\t'.format(i+1, j+1, p.name, len(p.argv))
                potentials_block += '\t'.join('{:g}'.format(a) for a in p.argv)
                potentials_block += '\n'
        
        closures_block = """
        --CLOSURES
        i j name
        """
        closures_block = dedent(closures_block)
        for i, cc in enumerate(self.closures):
            for j, c in enumerate(cc):
                closures_block += '{:d} {:d} {:s}\n'.format(i+1, j+1, c)

        all_blocks = ''.join([iteration_block, plan_block, species_block, potentials_block, closures_block])
        conf_text = dedent(all_blocks)
        
        if hasattr(fname, 'write'):
            fname.write(conf_text)
        else:
            with open(fname, 'w') as f:
                f.write(conf_text)

        return
