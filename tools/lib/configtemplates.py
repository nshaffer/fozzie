#!/usr/bin/env python
from __future__ import division
from .fozzieconfig import *

#-------------------------------------------------------------------------------

def ocp_config(Gamma, rolloff=1.08, solver='NORMAL', max_iterations=1000, tolerance=1e-8,
               mix=1.0, num_gridpoints=2**14-1, dr=5e-3):
    """Create a configuration for a one-component plasma calculation.
    
    Example usage with `genconf.py`:

        $ python genconf.py ocp 1.0 > gamma_1.in
        $ python genconf.py ocp 1.0 tolerance=3e-12 > gamma_1.in
    """

    species = [Species(charge=1.0, mass=1.0, conc=1.0, temperature=1.0)]
    potentials = [[CoulombPotential(Gamma, rolloff)]]
    closures = [['HNC']]
    conf = FozzieConfiguration(solver, max_iterations, tolerance, mix, 
                               num_gridpoints, dr, 
                               species, potentials, closures)
    return conf

#-------------------------------------------------------------------------------

def yocp_config(Gamma, kappa, rolloff=2.16, solver='NORMAL', max_iterations=1000, tolerance=1e-8,
                mix=1.0, num_gridpoints=2**14-1, dr=5e-3):
    """Create a configuration for a screened one-component plasma calculation.
    
    Example usage with `genconf.py`:

       $ python genconf.py yocp 45.3 1.2 > yocp.in
       $ python genconf.py yocp 45.3 1.2 dr=1e-4 > yocp.in
    """

    species = [Species(charge=1.0, mass=1.0, conc=1.0, temperature=1.0)]
    potentials = [[YukawaPotential(Gamma, kappa, rolloff)]]
    closures = [['HNC']]
    conf = FozzieConfiguration(solver, max_iterations, tolerance, mix, 
                               num_gridpoints, dr, 
                               species, potentials, closures)
    return conf

#-------------------------------------------------------------------------------

def bim_config(Gamma, z, m, x_or_y, composition='mole', rolloff=1.08, solver='NORMAL', 
               max_iterations=1000, tolerance=1e-8, mix=1.0, num_gridpoints=2**14-1, dr=5.e-3):
    """Create a configuration for a binary ionic mixture calculation.

    The coupling strength is defined by ``Gamma=<Z>^1/3 <Z^5/3> Gamma_0``, 
    where ``Gamma_0=e^2/akT`` and angle brackets mean, e.g., 
    ``<A> = x1*A1 + x2*A2``. 

    The composition of the mixture can be specified in terms of either
    mole fractions (``x_i=n_i/n``) or mass fractions (``y_i=rho_i/rho``).
    
    Example usage with `genconf.py`:

        $ python genconf.py bim 5.3 [1,2] [1,4] [0.75,0.25] > H_He.in
        $ python genconf.py bim 25 [1,6] [1,12] [0.9,0.1] composition=\\'mass\\' rolloff=0.54 > H_C.in
    """
    assert len(z) == 2, "Must specify exactly two charges. Got %d instead." % len(z)
    z = z1, z2 = [float(_) for _ in z]

    assert len(m) == 2, "Must specify exactly two masses. Got %d instead." % len(m)
    m = m1, m2 = [float(_) for _ in m]

    assert len(x_or_y) == 2, "Must specify exactly two mole/mass fractions. Got %d instead." % len(x_or_y)
    composition = composition.lower()
    if composition == 'mole':
        x = x1, x2 = [float(_) for _ in x_or_y]
    elif composition == 'mass':
        y1, y2 = [float(_) for _ in x_or_y]
        x1 = 1.0 / (1 + (y2/y1)*(m1/m2))
        x2 = 1.0 / (1 + (y1/y2)*(m2/m1))
        x = x1, x2
    else:
        msg = "Unsupported composition type: %s. Expected 'mole' or 'mass'." % composition
        raise ValueError(msg)

    species = [Species(charge=z_, mass=m_, conc=x_, temperature=1.0) 
               for z_, m_, x_ in zip(z, m, x)]

    ave_z = x1*z1 + x2*z2
    ave_z53 = x1*z1**(5./3) + x2*z2**(5./3)
    Gamma_0 = Gamma / ave_z**(1./3) / ave_z53
    potentials = [[CoulombPotential(zi*zj*Gamma_0, rolloff) for zj in z] 
                                                            for zi in z]

    closures = [['HNC']*2]*2
    conf = FozzieConfiguration(solver, max_iterations, tolerance, mix,
                               num_gridpoints, dr,
                               species, potentials, closures)
    return conf

#-------------------------------------------------------------------------------

def ei_deutsch_config(Gamma_i, Gamma_e, L, rolloff=1.08, solver='SVT', max_iterations=10000,
                      tolerance=1e-8, mix=0.1, num_gridpoints=2**14-1, dr=5e-3, mass_ratio=1836.0):
    """Create a configuration for an e-i Deutsch calculation, possibly with two temperatures.

    Example usage with `genconf.py`:

       $ python genconf.py ei_deutsch 5 1.2 0.8 > ei_twotemp.in
       $ python genconf.py ei_deutsch 5 5 0.8 solver=\\'NORMAL\\' > ei_eq.in
    """
    Te, Ti = Gamma_i/Gamma_e, 1.0
    me, mi = 1.0, mass_ratio

    e = Species(charge=-1.0, mass=me, conc=0.5, temperature=Te) 
    i = Species(charge= 1.0, mass=mi, conc=0.5, temperature=Ti)
    species = [i, e]

    epot_ee = Gamma_e/e.conc**(1.0/3.0)
    epot_ii = Gamma_i/i.conc**(1.0/3.0)
    epot_ie = -(epot_ee*epot_ii)**0.5 \
              * (Te*Ti)**0.5 / ( (mi*Te + me*Ti)/(mi + me) )
    potentials = [[CoulombPotential(epot_ii, rolloff), DeutschPotential(epot_ie, 1./L, rolloff)],
                  [DeutschPotential(epot_ie, 1./L, rolloff), CoulombPotential(epot_ee, rolloff)]]

    closures = [['HNC']*2]*2
    conf = FozzieConfiguration(solver, max_iterations, tolerance, mix, 
                               num_gridpoints, dr, 
                               species, potentials, closures)
    return conf

#-------------------------------------------------------------------------------

def posion_config(Gamma_i, Gamma_e, mi_me, rolloff=1.08, solver='SVT', max_iterations=1000,
                  tolerance=1e-8, mix=0.5, num_gridpoints=2**14-1, dr=5e-3):
    """Create a configuration for a positron-ion calculation, possibly with two temperatures.

    Example usage with `genconf.py`:

       $ python genconf.py posion 5 1.2 1836 > posion_twotemp.in
       $ python genconf.py posion 5 5 10 solver=\\'NORMAL\\' > posion_eq.in
    """

    Te, Ti = Gamma_i/Gamma_e, 1.0
    me, mi = 1.0, mi_me*1.0

    e = Species(charge=1.0, mass=me, conc=0.5, temperature=Te) 
    i = Species(charge=1.0, mass=mi, conc=0.5, temperature=Ti)
    species = [i, e]

    epot_ee = Gamma_e/e.conc**(1.0/3.0)
    epot_ii = Gamma_i/i.conc**(1.0/3.0)
    epot_ie = (epot_ee*epot_ii)**0.5 \
              * (Te*Ti)**0.5 / ( (mi*Te + me*Ti)/(mi + me) )
    potentials = [[CoulombPotential(epot_ii, rolloff), CoulombPotential(epot_ie, rolloff)],
                  [CoulombPotential(epot_ie, rolloff), CoulombPotential(epot_ee, rolloff)]]

    closures = [['HNC']*2]*2
    conf = FozzieConfiguration(solver, max_iterations, tolerance, mix, 
                               num_gridpoints, dr, 
                               species, potentials, closures)
    return conf

#-------------------------------------------------------------------------------

def hs_config(sigma, solver='NORMAL', max_iterations=1000, tolerance=1e-8,
               mix=1.0, num_gridpoints=2**14-1, dr=5e-3):
    """Create a configuration for a hard-sphere calculation."""

    species = [Species(charge=0.0, mass=1.0, conc=1.0, temperature=1.0)]
    potentials = [[HardSpherePotential(sigma)]]
    closures = [['PY']]
    conf = FozzieConfiguration(solver, max_iterations, tolerance, mix, 
                               num_gridpoints, dr, 
                               species, potentials, closures)
    return conf
