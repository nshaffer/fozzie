#!/usr/bin/env python
import sys
from lib.configtemplates import *

runtype = sys.argv[1].lower()
runargs = sys.argv[2:]

# ---- WARNING ----
# unsafe eval() usage below
cmdfunc = '{:s}_config'.format(runtype)
cmdargs = ', '.join('{:s}'.format(arg) for arg in runargs)
cmd = cmdfunc + '(' + cmdargs + ')'
conf = eval(cmd)
conf.write_to(sys.stdout)
    
"""Examples:

$ python genconf.py ocp 1.0 > conf.in
$ python genconf.py ocp 1.0 tolerance=1e-12 > conf.in
$ python genconf.py ocp 1.0 solver=\'SVT\' > conf.in  [[Note the escaped quotes!]]
$ python genconf.py "sys.stdout.write('hacked') #" 
"""
