#!/usr/bin/env python
import shutil
import subprocess
import sys
from lib.configtemplates import ei_deutsch_config

FOZPATH = '~/sw/source_files/fozzie/'


def run_fozzie(infile, guessfile, prefix, logfile):
    command = FOZPATH + 'foz'
    command += ' --config-file {:s} --prefix {:s}'.format(infile, prefix)
    if guessfile:
        command += ' --guess-file {:s}'.format(guessfile)
    output = subprocess.check_output(command, shell=True, stderr=subprocess.STDOUT)
    with open(logfile, 'w') as f:
        f.write(output)
    return
    
    
def main(): 
    Gm1 = float(sys.argv[1])
    Gm2 = float(sys.argv[2])
    L = float(sys.argv[3])
    mix = 0.1
    dr = 0.0015 
    N = 2**15-1

    Lscale = 0.9
    n = 0

    header = '#{:>3s} {:>10s} {:>10s} {:>10s} {:>10s}'.format('id', 'Gm1', 'Gm2', 'L/a', 'mix')
    summary_string = ' {:3d} {:10f} {:10f} {:10f} {:10f}'
    
    # perform initial run
    prefix = '{:0>2d}.'.format(n)
    conf_fname = prefix+'in'
    guess_fname = ''
    log_fname = prefix+'log'
    print header
    print summary_string.format(n, Gm1, Gm2, L, mix)
    conf = ei_deutsch_config(Gm1, Gm2, L, mix=mix, dr=dr, num_gridpoints=N)
    conf.write_to(conf_fname)
    run_fozzie(conf_fname, guess_fname, prefix, log_fname)
    shutil.copy2(prefix+'csr.dat', 'csr.in')

    
    while (n < 50 and Lscale < 0.995):
        n += 1
        L *= Lscale
        try:
            # Attempt to run
            prefix = '{:0>2d}.'.format(n)
            conf_fname = prefix+'in'
            guess_fname = 'csr.in'
            log_fname = prefix+'log'
            print summary_string.format(n, Gm1, Gm2, L, mix)
            conf = ei_deutsch_config(Gm1, Gm2, L, mix=mix, dr=dr, num_gridpoints=N)
            conf.write_to(conf_fname)
            run_fozzie(conf_fname, guess_fname, prefix, log_fname)
            shutil.copy2(prefix+'csr.dat', 'csr.in')
        except subprocess.CalledProcessError:
            # rerun with smaller decrement in L
            # and less agressive mix
            L /= Lscale
            Lscale = 0.5*(1.0 + Lscale)
            mix = 0.01
            

        
if __name__ == '__main__':
    main()    
