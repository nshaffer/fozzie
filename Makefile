FC=gfortran
#OPTIMIZE=-g -O0
OPTIMIZE=-O3
FFLAGS=$(OPTIMIZE) -std=f2008 -Wall -Wextra -pedantic -fimplicit-none -fbounds-check -fcheck-array-temporaries -ffpe-trap=invalid,zero,overflow -fbacktrace -fmax-errors=5 -Imod -Jmod

LDLIBS=-L${HOME}/sw/lib -ldfftpack 

TARGET=foz

SOURCES=src/kinds.f90 \
	src/constants.f90 \
	src/radialft.f90 \
	src/tinymatrix.f90 \
	src/species.f90 \
	src/potentials.f90 \
	src/closure.f90 \
	src/iteration_config.f90 \
	src/oz.f90 src/input.f90 \
	src/cmdline.f90 \
	src/main.f90
MODULES:=$(patsubst src/%.f90,mod/%.mod,$(SOURCES))
OBJECTS:=$(patsubst %.f90,%.o,$(SOURCES))

TEST_SRC:=$(wildcard tests/*_test.f90)
TESTS:=$(patsubst %_test.f90,%_test,$(TEST_SRC))

.PHONY : all info tests debug clean install

all : $(TARGET) $(OBJECTS) $(TESTS) 

$(TARGET) : $(OBJECTS)
	$(LINK.f) -o $@ $^ $(LDLIBS)  

%.o : %.f90
	$(COMPILE.f) -o $@ $^ $(LDLIBS)

tests : $(TESTS)

%_test : %_test.f90 $(OBJECTS) 
	$(LINK.f) -o $@ $^ $(LDLIBS)

clean :
	$(RM) $(MODULES)
	$(RM) $(OBJECTS)
	$(RM) $(TARGET)
	$(RM) $(TESTS)

info : 
	@echo "Compile CMD: "$(COMPILE.f)
	@echo "Link CMD: "$(LINK.f)
	@echo "Libraries: "$(LDLIBS)
	@echo
	@echo "Sources: "$(SOURCES)
	@echo "Modules: "$(MODULES)
	@echo "Objects: "$(OBJECTS)
	@echo 
	@echo "Test sources: "$(TEST_SRC)
	@echo "Tests: "$(TESTS)
